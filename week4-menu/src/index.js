function order() {

    //get input: Using box suffix to not actually
    //confuse it with the values below
    let veganBox = document.querySelector("#vegan");
    let cheeseBox = document.querySelector("#cheese");
    let tomatoeBox = document.querySelector("#tomatoe");
    let lettuceBox = document.querySelector("#lettuce");
    let baconBox = document.querySelector("#bacon");


    //processing:

    //Assume nothing is selected.
    let base_price = 5; //Note, not $ because computer don't understand units
    let burgerPrice = 0;
    let vegan = 0;
    let cheese = 0;
    let tomatoe = 0;
    let lettuce = 0;
    let bacon = 0;

    //use if statements to check if an option is selected
    //modify our pricing accordingly.
    if(veganBox.checked){
        vegan = 1;
    }
    //Notice no else, since we want all the checks to run
    if(cheeseBox.checked){
        cheese = 0.5;
    }
    if(tomatoeBox.checked){
        tomatoe = 1.20;
    }
    if(lettuceBox.checked){
        lettuce = 2.00;
    }
    if(baconBox.checked){
        bacon = 1.50;
    }
    //Final calculations.  If all out variables have the correct value
    //formula is always the same.
    burgerPrice = base_price + vegan + cheese + tomatoe + lettuce + bacon;

    //Output: Change the total
    let totalElement = document.querySelector("#total");
    //There is a property called text, which allows use to 
    //change the text
    totalElement.innerHTML= "$"+burgerPrice.toFixed(2);  
}

window.order = order;