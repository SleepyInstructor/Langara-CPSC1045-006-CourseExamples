console.log("Script is connected");

setInterval(moveTarget, 100); // times specified in MS

function setTarget() {
    let circle = document.querySelector(".cir");  //Selects the first circle

    circle.setAttribute("xTarget", event.offsetX);
    circle.setAttribute("yTarget", event.offsetY);

}
function moveTarget() {
    let circle = document.querySelector(".cir");  //gets the first element with class .cir
    let x = Number(circle.getAttribute("cx"));
    let y = Number(circle.getAttribute("cy"));
    let xTarget = Number(circle.getAttribute("xTarget"));
    let yTarget = Number(circle.getAttribute("yTarget"));

    x = x + (xTarget - x) * 0.1;  //It will never get there.
    y = y + (yTarget - y) * 0.1;

    circle.setAttribute("cx", x);
    circle.setAttribute("cy", y);

}

//Version two, which is better
//Does the same as above, but for more circles at once
//Circles after the first one, follows the circle in front of it.
function getAttributes(circle) {
    return {
        x: Number(circle.getAttribute("cx")),
        y: Number(circle.getAttribute("cy")),
        xTarget: Number(circle.getAttribute("xTarget")),
        yTarget: Number(circle.getAttribute("yTarget"))
    }
}
function move(element) {
    let attr = getAttributes(element);
    let x = attr.x + (attr.xTarget - attr.x) * 0.05;
    let y = attr.y + (attr.yTarget - attr.y) * 0.05;

    element.setAttribute("cx", x);
    element.setAttribute("cy", y);
}

function moveTargetV2() {  //Version 2, assumes we have more than one target
    let circles = document.querySelectorAll(".cir");  //gets all the circles in an array

    //Implement follow behavior
    for (let i = 1; i < circles.length; i += 1) {
        let xTarget = circles[i - 1].getAttribute("cx");
        let yTarget = circles[i - 1].getAttribute("cy");
        circles[i].setAttribute("xTarget", xTarget);
        circles[i].setAttribute("yTarget", yTarget);
    }

    //Move each circle closer to their targets
    for (let cir of circles) {

        move(cir);
    }

}