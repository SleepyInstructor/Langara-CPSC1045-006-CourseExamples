
let score = 0;
let x_cir = 200;
let y_cir = 200;
function move(){
 
    //Get inputs
    let xInput = document.querySelector("#rangeX");
    let yInput = document.querySelector("#rangeY");
    x = Number(xInput.value);
    y = Number(yInput.value);

    //processing: check if x and y are at the target
    let color;
    if((x >= x_cir-10 && x <= x_cir+10) && 
        (y >= y_cir-10 && y <= y_cir +10)){
        color = "red";
        score = score +1;        
        //score += 1;
        //score++;

        //change circle location
        x_cir = Math.round(400*Math.random());
        y_cir = Math.round(400*Math.random());
    } else {
        color = "black";
    }
    
    if(score >= 5){
        console.log("you win!");
        score = 0;
    }

    //move the crosshair and change target color
    let crossHair = document.querySelector("#crossHair");
    let target = document.querySelector("#target");
    console.log("score", score);
    let translateString = "translate("+x+" "+y+")"; //create translate to move cross hair as one unit
    crossHair.setAttribute("transform", translateString);
    target.setAttribute("fill",color);
    target.setAttribute("cx",x_cir);
    target.setAttribute("cy",y_cir);

    
}

window.move = move;