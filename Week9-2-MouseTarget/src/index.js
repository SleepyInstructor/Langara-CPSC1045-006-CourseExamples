
let score = 0;
let x_cir = 200;
let y_cir = 200;
function newFunction() {
    console.log("test");
}
function fire() {
    let x = event.offsetX;
    let y = event.offsetY;
    let circle = event.target;
    let x_cir = Number(circle.getAttribute("cx"));
    let y_cir = Number(circle.getAttribute("cy"));
    let color = "black";
    if ((x >= x_cir - 10 && x <= x_cir + 10) &&
        (y >= y_cir - 10 && y <= y_cir + 10)) {
        color = "red";
        score = score + 1;
   

        //change circle location
        x_cir = Math.round(400 * Math.random());
        y_cir = Math.round(400 * Math.random());
        setTimeout(function () {
            circle.setAttribute("fill", "black");

        }, 500);
    }
    circle.setAttribute("fill", color);
    circle.setAttribute("cx", x_cir);
    circle.setAttribute("cy", y_cir);


}


function moveCursor() {
    let x = event.offsetX;
    let y = event.offsetY;
    let crossHair = document.querySelector("#crossHair");
    let translateString = "translate(" + x + " " + y + ")";
    crossHair.setAttribute("transform", translateString);
}

window.newFunction = newFunction;
window.fire = fire;
window.moveCursor = moveCursor;