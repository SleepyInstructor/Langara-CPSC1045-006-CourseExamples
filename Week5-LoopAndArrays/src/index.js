//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

let score = 0;
function setup() {
    console.log(event);//event object
    //Will code the rest in class to go with notes
}

function move() {

    //Get inputs
    let xInput = document.querySelector("#rangeX");
    let yInput = document.querySelector("#rangeY");
    x = Number(xInput.value);
    y = Number(yInput.value);
    
    //Get the targets, we will use the cx and cy stored in the targets
    //targets in this program are both input and output channels
    let targets = document.querySelectorAll(".target");  //This returns an arraylike object
    console.log("targets", targets); //So you can see how an array looks like the console

    //Processing section
    //Will code the rest in class to go with notes
    for(let target of targets){
        console.log("circle:",target);  //Less useful, you can see the individual targets

        //Get any inputs, we can ask this question more than once, imagine a cascade of questions
        let cir_x = target.getAttribute("cx");
        let cir_y = target.getAttribute("cy");

    }


    

    //Output section
    let crossHair = document.querySelector("#crossHair");
    let translateString = "translate("+x+" "+y+")"; //create translate to move cross hair as one unit
    crossHair.setAttribute("transform", translateString);
    let scoreOutput = document.querySelector("#score");
    scoreOutput.innerHTML = score;
}

window.setup = setup;
window.move = move;