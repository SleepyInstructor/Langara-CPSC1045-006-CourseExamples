//Print message to console
//for debugging
console.log("main.js executing");
sliderListener();

function sliderListener(event){
    let sliderX = document.querySelector("#rangeX");
    let sliderY = document.querySelector("#rangeY");
    let circle = document.querySelector("#circle");

    let x0 = Number(sliderX.value);
    let y0 = Number(sliderY.value);

    let x = x0;
    let y =  y0+((x0-150)*(x0-150))/100;

    circle.setAttribute("cx", x);
    circle.setAttribute("cy", y); 

    console.log(circle); //for debugging, so we can see the circle in the console
}


function changeColor(event){
   let colorPicker = document.querySelector("#color");
   let circle = document.querySelector("#shape");
   circle.setAttribute("fill", colorPicker.value);
}