//Print message to console
//for debugging
console.log("main.js executing");
sliderListener();

function sliderListener(event){
    let sliderX = document.querySelector("#rangeX");
    let sliderY = document.querySelector("#rangeY");
    let circle = document.querySelector("#shape");

    let x0 = Number(sliderX.value);
    let y0 = Number(sliderY.value);

    let translatesString = "translate("+x0+","+y0+" )";

    circle.setAttribute("transform", translatesString);

    console.log(circle); //for debugging, so we can see the circle in the console
}


function changeColor(event){
   let colorPicker = document.querySelector("#color");
   let circle = document.querySelector("#shape");
   circle.setAttribute("fill", colorPicker.value);
}