function balance() {
    //get inpt
    let wLInput = document.querySelector("#wL");
    let wRInput = document.querySelector("#wR");

    let wL = Number(wLInput.value);
    let wR = Number(wRInput.value);

    //Processing step
    //from input determine
    //color of boxes and 
    //angle of rotation
    let colorR, colorL, angle;

    if (wL < wR) {  //Right side is heavier
        colorR = "red";
        colorL = "blue";
        angle = 10;
    } else if (wL > wR) {  //Left side is heavier
        colorR = "blue";
        colorL = "red";
        angle = -10;
    } else if(wL == wR) { // case when they are equal
        colorR = "black";
        colorL = "black";
        angle = 0;
    }

    //Output:  Change attributes of the scale
    let transformString = "rotate("+angle+" 205 120)";  //Crete transform strings
    let scale = document.querySelector("#scale");
    let boxR = document.querySelector("#rectR");
    let boxL = document.querySelector("#rectL");

    scale.setAttribute("transform", transformString);
    boxR.setAttribute("fill", colorR);
    boxL.setAttribute("fill", colorL);

}
