//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function myFunction(){
   let sliderInput = document.querySelector("#myInput");
   console.log("sliderInput",sliderInput);
   console.log("slider.value",sliderInput.value);
   console.log("slider.value",Number(sliderInput.value));

   let val = Number(sliderInput.value);

   let rectHello = document.querySelector("#rect1");
   let newVal = 100+val;
   console.log("newVal",newVal);
   rectHello.setAttribute("width",  newVal);

}

window.myFunction = myFunction;